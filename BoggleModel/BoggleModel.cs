﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomNetworking;
using System.Threading;
using System.Net.Sockets;

namespace BoggleModel
{
    /// <summary>
    /// The Boggle client model which is primarily used by the Boggle GUI.
    /// 
    /// -David Onken and Grey Leman
    /// </summary>
    public class BoggleGameModel
    {
        // Stringsocket for server communication

        /// <summary>
        /// Contains the String Socket for this model. Privately set.
        /// </summary>
        public StringSocket socket { get; private set;}

        /// <summary>
        /// Contains the nickname of this player.
        /// </summary>
        public string nickname { get; private set;}

        // Stored to kill this socket easily
        private TcpClient serverSocket;

        /// <summary>
        /// Used to register events for the model
        /// </summary>
        public event Action<String> IncomingLineEvent;

        /// <summary>
        /// Stores the default text to append to the "local player" in the GUI
        /// </summary>
        public string playerText;

        /// <summary>
        /// Stores the default text to append to the "opponent" in the GUI
        /// </summary>
        public string opponentText;

        /// <summary>
        /// Boolean which denotes if this client is currently involved in a game or not
        /// </summary>
        public bool gameInProgress;

        /// <summary>
        /// Denotes the time first transmitted by the server.
        /// </summary>
        public int time;

        /// <summary>
        /// Creates a not yet connected client model. 
        /// </summary>
        public BoggleGameModel()
        {
            // Initialize
            socket = null;
            nickname = "John";
            playerText = "";
            opponentText = "";
            serverSocket = null;
        }

        /// <summary>
        /// Disconnects the model (player) from the server immediately.
        /// </summary>
        public void disconnect()
        {
            // Disconnect by nulling the socket and falsify the boolean
            socket = null;
            gameInProgress = false;

            try
            {
                serverSocket.Close();
            }
            catch (NullReferenceException) { return; }
            serverSocket = null;
        }

        /// <summary>
        /// Attempts to establish a connection to the input IPAddress and name function. Returns a boolean
        /// of true if successful.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="IPAddress"></param>
        /// <returns></returns>
        public bool attemptConnect(string name, string IPAddress)
        {
            if (gameInProgress == true)
                return false;

            IPAddress = IPAddress.Trim();

            if (IPAddress.ToUpper().Equals("LOCALHOST"))
                IPAddress = "127.0.0.1";

            // Attempt to connect to the server
            try
            {
                System.Net.IPAddress.Parse(IPAddress);
                serverSocket = new TcpClient(IPAddress, 2000);
                socket = new StringSocket(serverSocket.Client, UTF8Encoding.Default);
            }
            catch { return false; }

            // If connection was successful, then invert the boolean and return true
            gameInProgress = true;
            nickname = name;
            successfulConnect();

            return true;
        }

        private void successfulConnect()
        {
            
            // If successfully connected, then send the PLAY command and start listening for things.
            socket.BeginSend("PLAY " + nickname + "\n", (e, p) => { }, null);
            socket.BeginReceive(LineReceived, null);
        }

        /// <summary>
        /// Sends a string to the connected server.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool sendLine(string s)
        {
            // Send the word command, check if it was successful
            try
            {
                socket.BeginSend("WORD " + s + "\n", (e, o) => { }, null);
            }
            catch { return false; }
            return true;
        }

        private void LineReceived(String s, Exception e, object p)
        {
            // If the socket is null, break the thread
            if (socket == null || gameInProgress == false)
                return;

            // Call the line event
            if (IncomingLineEvent != null)
            {
                IncomingLineEvent(s);
            }

            // If the socket is null, break the thread
            if (socket == null || gameInProgress == false)
                return;

            // Loop back in
            socket.BeginReceive(LineReceived, null);
        }      


    }
}
