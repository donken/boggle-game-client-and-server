﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BoggleLogin
{
    public partial class Form1 : Form
    {
        public bool connectionRequested;

        public string IPAddress;
        public string port;
        public string nickname;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPAddress = textBox1.Text.Trim();
            port = textBox2.Text.Trim();
            nickname = textBox3.Text.Trim();

            connectionRequested = true;

        }
    }
}
