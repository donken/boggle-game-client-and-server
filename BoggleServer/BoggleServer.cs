﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using CustomNetworking;
using System.Timers;
using BB;

namespace ServerUtilities
{
    public class BoggleServer
    {
        /// <summary>
        /// Representation Invariant
        /// - seconds and letters are provided by the user and are consistent for all games played on a single server
        /// - pendingPlayer determines whether a game is waiting on player2 to connect
        /// - legalWords contains a list of all possible legal words
        /// - server accepts sockets on port 2000 and administers them to string sockets as clients connect
        /// - stringSocket will always be the socket given to the player who next connects, and will be created with a new socket once it has been given away
        /// - closeServer will tell an updater method to close the server
        /// - currentGame is forgotten once two players have connected and reassigned when a new game is requested by a third, fifth, seventh, etc... player
        /// </summary>
        private string seconds, letters;
        private bool pendingPlayer = false;
        private Dictionary<string, bool> legalWords;
        private TcpListener server;
        private StringSocket stringSocket;
        private bool closeServer = false;
        private Game currentGame;

        /// <summary>
        /// Two parameter constructor containing a time and filepath to a dictionary
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="filePath"></param>
        public BoggleServer(string seconds, string filePath)
        {
            setUpServer(seconds, filePath, "");
        }

        /// <summary>
        /// Three parameter constructor containing a time, filepath to a dictonary, and set of letters to use as a board
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="filePath"></param>
        /// <param name="letters"></param>
        public BoggleServer(string seconds, string filePath, string letters)
        {
            setUpServer(seconds, filePath, letters);
        }

        /// <summary>
        /// Sets up the server with the parameters provided by the constructor
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="filePath"></param>
        /// <param name="letters"></param>
        private void setUpServer(string seconds, string filePath, string letters)
        {
            legalWords = new Dictionary<string, bool>();

            this.seconds = seconds;
            this.letters = letters;

            loadDictionary(filePath);

            server = new TcpListener(IPAddress.Any, 2000);
            server.Start();
            server.BeginAcceptSocket(socketAccepted, null);
        }
        /// <summary>
        /// This method is called whenever a new socket is accepted
        /// If the server is told to close, this is where it is closed
        /// </summary>
        /// <param name="a"></param>
        private void socketAccepted(IAsyncResult a)
        {
            if (closeServer)
            {
                server.EndAcceptSocket(a);
                server.Stop();
                server = null;
                stringSocket = null;
                return;
            }
            stringSocket = new StringSocket(server.EndAcceptSocket(a), new UTF8Encoding());
            stringSocket.BeginReceive(ConnectionEstablished, stringSocket);
            server.BeginAcceptSocket(socketAccepted, null);
        }

        /// <summary>
        /// Called once the current string socket has been connected. If it is a player1 connecting, then the game is created anew. If it is a player2 connecting, then pendingPlayer becomes false and the game is forgotten by the server
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void ConnectionEstablished(string s, Exception e, object payload)
        {
            if (!pendingPlayer)
            {
                pendingPlayer = true;
                currentGame = new Game(this.seconds, this.letters, this.legalWords);
                currentGame.addPlayer(s, payload as StringSocket);
            }
            else
            {
                currentGame.addPlayer(s, payload as StringSocket);
                pendingPlayer = false;
            }
        }

        /// <summary>
        /// Loads a dictionary to be provided to all games
        /// </summary>
        /// <param name="filePath"></param>
        private void loadDictionary(string filePath)
        {
            StreamReader streamReader = new StreamReader(filePath);

            string line = "";

            while ((line = streamReader.ReadLine()) != null)
            {
                legalWords.Add(line, true);
            }

            streamReader.Close();
        }

        /// <summary>
        /// Tells the server to close
        /// </summary>
        public void CloseServer()
        {
            closeServer = true;
        }

        /// <summary>
        /// Sets up a server with command line arguments. The server is closed once return is entered
        /// NOTE - COMMAND LINE ARGUMENTS MAY DIFFER FROM MACHINE TO MACHINE IF THE DICTIONARY'S LOCATION IS NOT CONSISTENT
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            if (args[0] == null || args[1] == null)
            {
                return;
            }
            BoggleServer server;
            if (args.Length < 3)
            {
                server = new BoggleServer(args[0], args[1]);
            }
            else
            {
                server = new BoggleServer(args[0], args[1], args[2]);
            }
            Console.ReadLine();
            server.CloseServer();
        }
    }
}
