﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CustomNetworking;
using BB;

namespace ServerUtilities
{
    public class Game
    {
        /// <summary>
        /// Representation Invariant
        /// - player1 will always be assigned before player2 and the game will not start until player 2 has connected
        /// - player1Receiving and player2Receiving will only be false if a valid name has been input and will be set in the add player and other starting methods
        /// - player1Score and player2Score are set according to the rules of boggle whenever a valid or invalid word is played
        /// - player1Words and player2Words are a sum total of all legal and illegal words played by each player
        /// - timeRemaining will be deducted from every second. The class is set up to end once the variable has reached -1
        /// - letters will compromise the boggle board
        /// - board contains all words and is used to check if valid words are present
        /// - timer will deduct one from the timeRemaining every second
        /// - wordDictionary holds all valid words possible
        /// - sharedWords contains words both players have played
        /// - gameOver is only true once the game has ended
        /// </summary>
        private Player player1;
        private Player player2;
        private bool player1Receiving;
        private bool player2Receiving;
        private int player1Score = 0;
        private int player2Score = 0;
        private Dictionary<string, bool> player1Words;
        private Dictionary<string, bool> player2Words;
        private int timeRemaining;
        private string letters;
        private BoggleBoard board;
        private System.Threading.Timer timer;
        private Dictionary<string, bool> wordDictionary;
        private LinkedList<string> sharedWords;
        private bool gameOver = false;

        readonly object gameLock = new object();

        /// <summary>
        /// Constructor for a game
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="letters"></param>
        /// <param name="wordDictionary"></param>
        public Game(string seconds, string letters, Dictionary<string, bool> wordDictionary)
        {
            player1Words = new Dictionary<string, bool>();
            player2Words = new Dictionary<string, bool>();

            this.letters = letters;

            buildBoard(seconds, letters);

            this.wordDictionary = wordDictionary;

            sharedWords = new LinkedList<string>();
        }

        /// <summary>
        /// Adds a player, gives them a name contained within s and a string socket and begins detecting when the game should start
        /// </summary>
        /// <param name="s"></param>
        /// <param name="stringSocket"></param>
        public void addPlayer(string s, StringSocket stringSocket)
        {
            lock (gameLock)
            {
                if (player1 == null)
                {
                    player1 = new Player("", stringSocket);
                    startOneReady(s, null, null);
                }
                else
                {
                    if (player2 == null)
                    {
                        player2 = new Player("", stringSocket);
                        startTwoReady(s, null, null);
                    }
                    else
                    {
                        throw new Exception("Game is full");
                    }
                }
            }
        }

        /// <summary>
        /// Starts the game if both players are present and have valid names
        /// </summary>
        private void startGame()
        {
            if (player1 == null || player2 == null) return;
            if (player1Receiving == false && player2Receiving == false)
            {
                player1.getStringSocket().BeginSend("START " + letters + " " + timeRemaining.ToString() + " " + player2.getName() + '\n', (f, o) => { }, null);
                player2.getStringSocket().BeginSend("START " + letters + " " + timeRemaining.ToString() + " " + player1.getName() + '\n', (f, o) => { }, null);
                player1.getStringSocket().BeginReceive(player1Received, null);
                player2.getStringSocket().BeginReceive(player2Received, null);
                timer = new System.Threading.Timer(updateTimer, null, 0, 1000);
            }
        }

        /// <summary>
        /// Handles detecting if player1 is ready to play
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void startOneReady(string s, Exception e, object payload)
        {
            if (s != null && (s.Length < 5 || s.Substring(0, 5) != "PLAY " || s.Trim() == "PLAY"))
            {
                player1Receiving = true;
                player1.getStringSocket().BeginSend("IGNORING " + s + '\n', (f, o) => { }, null);
                player1.getStringSocket().BeginReceive(startOneReady, null);
            }
            else
            {
                if (player2 != null && s.Substring(5) == player2.getName())
                {
                    startOneReady("IDENTICAL NAME", null, null);
                    return;
                }
                player1Receiving = false;
                player1.setName(s.Substring(5));
                startGame();
            }
        }

        /// <summary>
        /// Handles detecting if player2 is ready to play
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void startTwoReady(string s, Exception e, object payload)
        {
            if (s != null && (s.Length < 5 || s.Substring(0, 5) != "PLAY " || s.Trim() == "PLAY"))
            {
                player2Receiving = true;
                player2.getStringSocket().BeginSend("IGNORING " + s + '\n', (f, o) => { }, null);
                player2.getStringSocket().BeginReceive(startTwoReady, null);
            }
            else
            {
                if (player1 != null && s.Substring(5) == player1.getName())
                {
                    startTwoReady("IDENTICAL NAME", null, null);
                    return;
                }
                player2Receiving = false;
                player2.setName(s.Substring(5));
                startGame();
            }
        }

        /// <summary>
        /// Called every second and determines and handles the game's end as well as sends the time to the players
        /// </summary>
        /// <param name="source"></param>
        private void updateTimer(object source)
        {
            timeRemaining--;
            if (timeRemaining < 0)
            {
                timer.Dispose();
                timer = null;
                gameOver = true;
                //Console.WriteLine("GAME OVER");

                if (player1 != null && player2 != null)
                {
                    player1.getStringSocket().BeginSend("STOP " + player1.getLegalWordCount() + " " +
                                                                    player1.getLegalWords() +
                                                                    player2.getLegalWordCount() + " " +
                                                                    player2.getLegalWords() +
                                                                    sharedWords.Count.ToString() + " " +
                                                                    getSharedWords() +
                                                                    player1.getIllegalWordCount() + " " +
                                                                    player1.getIllegalWords() +
                                                                    player2.getIllegalWordCount() + " " +
                                                                    player2.getIllegalWords() + '\n', (e, o) => { player1.getStringSocket().closeStringSocket(); }, null);
                    player2.getStringSocket().BeginSend("STOP " + player2.getLegalWordCount() + " " +
                                                                    player2.getLegalWords() +
                                                                    player1.getLegalWordCount() + " " +
                                                                    player1.getLegalWords() +
                                                                    sharedWords.Count.ToString() + " " +
                                                                    getSharedWords() +
                                                                    player2.getIllegalWordCount() + " " +
                                                                    player2.getIllegalWords() +
                                                                    player1.getIllegalWordCount() + " " +
                                                                    player1.getIllegalWords() + '\n', (e, o) => { player2.getStringSocket().closeStringSocket(); }, null); 
                }
            }
            else
            {
                player1.getStringSocket().BeginSend("TIME " + timeRemaining.ToString() + '\n', (e, o) => { }, null);
                player2.getStringSocket().BeginSend("TIME " + timeRemaining.ToString() + '\n', (e, o) => { }, null);
            }
        }

        /// <summary>
        /// A test method
        /// </summary>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void playerDisconnected(Exception e, object payload)
        {
            //Console.WriteLine((payload as string) + " HAS DISCONNECTED");
        }

        /// <summary>
        /// Returns a list of shared words
        /// </summary>
        /// <returns></returns>
        private string getSharedWords()
        {
            string retVal = "";

            foreach (string s in sharedWords)
            {
                retVal += s + " ";
            }

            return retVal;
        }

        /// <summary>
        /// Handles when player1 has received a command from the client
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void player1Received(string s, Exception e, object payload)
        {
            if (s == null)
            {
                if (player2 != null) player2.getStringSocket().BeginSend("TERMINATED\n", playerDisconnected, player1.getName());
                player1 = null;
                timeRemaining = -1;
                return;
            }
            if (s.Substring(0, 5) == "WORD ")
            {
                recalculatePlayer1Score(s.Substring(5));
            }
            else
            {
                player1.getStringSocket().BeginSend("IGNORING " + s + '\n', (f, o) => { }, null);
            }
            if(!gameOver) player1.getStringSocket().BeginReceive(player1Received, null);
        }

        /// <summary>
        /// Recalculates player1's score
        /// </summary>
        /// <param name="word"></param>
        private void recalculatePlayer1Score(string word)
        {
            word = word.ToUpper();
            word = word.Trim();
            if (player2Words.ContainsKey(word) == false)
            {
                if (word.Length < 3) { player1.addIllegalWord(word); return; }
                player2Words.Add(word, false);
                player1Words.Add(word, true);
                if ((wordDictionary.ContainsKey(word) != true || !board.CanBeFormed(word)) && !player1.getIllegalWords().Contains(word))
                {
                    player1Score--;
                    player1.addIllegalWord(word);
                }
                else if (word.Length < 5)
                {
                    player1Score++;
                }
                else if (word.Length < 7)
                {
                    player1Score += word.Length - 3;
                }
                else if (word.Length == 7)
                {
                    player1Score += 5;
                }
                else player1Score += 11;

                player1.addLegalWord(word);

                player1.getStringSocket().BeginSend("SCORE " + player1Score.ToString() + " " + player2Score.ToString() + '\n', (e, o) => { }, null);
                player2.getStringSocket().BeginSend("SCORE " + player2Score.ToString() + " " + player1Score.ToString() + '\n', (e, o) => { }, null);
            }
            else
            {
                sharedWords.AddLast(word);
            }
        }

        /// <summary>
        /// Handles when player2 receives a command from the client
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="payload"></param>
        private void player2Received(string s, Exception e, object payload)
        {
            if (s == null)
            {
                if(player1 != null) player1.getStringSocket().BeginSend("TERMINATED\n", playerDisconnected, player2.getName());
                player2 = null;
                timeRemaining = -1;
                return;
            }
            if (s.Substring(0, 5) == "WORD ")
            {
                recalculatePlayer2Score(s.Substring(5));
            }
            else
            {
                player2.getStringSocket().BeginSend("IGNORING " + s + '\n', (f, o) => { }, null);
            }
            if(!gameOver) player2.getStringSocket().BeginReceive(player2Received, null);
        }

        /// <summary>
        /// Recalculates player2's score
        /// </summary>
        /// <param name="word"></param>
        private void recalculatePlayer2Score(string word)
        {
            word = word.ToUpper();
            word = word.Trim();
            if (player1Words.ContainsKey(word) == false)
            {
                if (word.Length < 3) { player2.addIllegalWord(word); return; }
                player1Words.Add(word, false);
                player2Words.Add(word, true);
                if ((wordDictionary.ContainsKey(word) != true || !board.CanBeFormed(word)) && !player2.getIllegalWords().Contains(word))
                {
                    player2Score--;
                    player2.addIllegalWord(word);
                }
                else if (word.Length < 5)
                {
                    player2Score++;
                }
                else if (word.Length < 7)
                {
                    player2Score += word.Length - 3;
                }
                else if (word.Length == 7)
                {
                    player2Score += 5;
                }
                else player2Score += 11;

                player2.addLegalWord(word);

                player1.getStringSocket().BeginSend("SCORE " + player1Score.ToString() + " " + player2Score.ToString() + '\n', (e, o) => { }, null);
                player2.getStringSocket().BeginSend("SCORE " + player2Score.ToString() + " " + player1Score.ToString() + '\n', (e, o) => { }, null);
            }
            else
            {
                sharedWords.AddLast(word);
            }
        }

        /// <summary>
        /// Creates the boggle board and throws and exception if the time parameter is invalid
        /// </summary>
        /// <param name="seconds"></param>
        /// <param name="letters"></param>
        private void buildBoard(string seconds, string letters)
        {
            if (letters != "") board = new BoggleBoard(letters);
            else board = new BoggleBoard();

            letters = board.ToString();
            try
            {
                timeRemaining = Int32.Parse(seconds);
                timeRemaining = Math.Abs(timeRemaining);
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a valid time value");
            }
        }
    }
}
