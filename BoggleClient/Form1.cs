﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using CustomNetworking;
using System.Net.Sockets;
using BoggleModel;


namespace BoggleClient
{
    public partial class Form1 : Form
    {

        BoggleGameModel model;

        TextBox[] textboxes;
        Button[] buttons;

        public Form1()
        {
            InitializeComponent();

            model = new BoggleGameModel();

            // Store the items in arrays
            textboxes = new TextBox[] { textBox1 };
            buttons = new Button[] {button1, button2, button3, button4, button5,
                button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16};
            // Set up the board
            disableAll();
            model.gameInProgress = false;

        }

        /// <summary>
        /// Method which is used to bridge the Model and View
        /// </summary>
        /// <param name="s"></param>
        private void received(string s)
        {
            // If s is null or the game boolean is false, then exit
            if (s == null || model.gameInProgress == false)
            {
                model.disconnect();
                return;
            }

            // Split the string
            string[] receive = s.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // If Terminated
            if (receive[0].ToUpper().Equals("TERMINATED"))
            {
                MessageBox.Show("This game was unexpectedly terminated by the server. You may begin a new game after exiting this box.");
                model.disconnect();
                TimeBar.Invoke(new Action(() => { TimeBar.Value = 0; }));
                TimeLeft.Invoke(new Action(() => { TimeLeft.Text = "-----"; }));
                Invoke(new Action(() => { disableAll(); }));
                return;
            }

            // If START
            if (receive[0].ToUpper().Equals("START"))
            {
                // Get the board
                char[] characters = receive[1].Trim().ToUpper().ToCharArray();
                try
                {
                    // Get the integer
                    model.time = Int32.Parse(receive[2]);
                }
                catch (FormatException)
                { return; }

                // Build the Opponent
                model.opponentText = receive[3] + ": ";
                Opponent.Invoke(new Action(() => { Opponent.Text = model.opponentText + "0"; }));

                if (characters.Length != 16)
                    return;

                // Build Board
                int index = 0;
                foreach (Button b in buttons)
                {
                    b.Invoke(new Action(() => { b.Text = characters[index++].ToString(); }));
                }

                return;

            }

            // Time Possiblity
            if (receive[0].ToUpper().Equals("TIME"))
            {
                // Calculate the time and apply it to the time left and time bar
                int num = 0;
                try
                {
                    num = Int32.Parse(receive[1]);
                    TimeBar.Invoke(new Action(() => { TimeBar.Value = (int)(((double)num / model.time) * 100); }));
                }
                catch (FormatException) { return; }

                TimeLeft.Invoke(new Action(() => { TimeLeft.Text = "" + num; }));
                return;
            }

            // Score Possiblity
            if (receive[0].ToUpper().Equals("SCORE"))
            {
                // Change scores
                LocalPlayer.Invoke(new Action(() => { LocalPlayer.Text = model.playerText + receive[1]; }));
                Opponent.Invoke(new Action(() => { Opponent.Text = model.opponentText + receive[2]; }));
            }

            // If stop
            if (receive[0].ToUpper().Equals("STOP"))
            {
                // Disconnect and revert all elements to start conditions
                model.disconnect();
                Opponent.Invoke(new Action(() => { Opponent.Text = "Opponent:"; }));
                Invoke(new Action(() => { disableAll(); }));

                // Make an array of linked lists
                LinkedList<string>[] lists = new LinkedList<string>[] { new LinkedList<string>(), new LinkedList<string>(), new LinkedList<string>(), new LinkedList<string>(), new LinkedList<string>() };

                int result = 0;
                int listNum = 0;

                for (int i = 2; i < receive.Length; i++)
                {
                    if (!Int32.TryParse(receive[i], out result))
                    {
                        lists[listNum].AddLast(receive[i]);
                    }
                    else
                        listNum++;
                }

                // Build the lists for the end message

                string legalClient = "[";
                string legalOppo = "[";
                string common = "[";
                string illegalClient = "[";
                string illegalOppo = "[";
                string separator = "";

                foreach (string next in lists[0])
                {
                    legalClient = legalClient + separator + next;
                    separator = ", ";
                }
                legalClient += "]";

                separator = "";

                foreach (string next in lists[1])
                {
                    legalOppo = legalOppo + separator + next;
                    separator = ", ";
                }

                legalOppo += "]";

                separator = "";

                foreach (string next in lists[2])
                {
                    common = common + separator + next;
                    separator = ", ";
                }

                common += "]";
                separator = "";

                foreach (string next in lists[3])
                {
                    illegalClient = illegalClient + separator + next;
                    separator = ", ";
                }

                illegalClient += "]";
                separator = "";

                foreach (string next in lists[4])
                {
                    illegalOppo = illegalOppo + separator + next;
                    separator = ", ";
                }
                illegalOppo += "]";

                // End message box
                string results = "RESULTS:\n\nLegal words by you: " + legalClient + "\n\nLegal words by opponent: " +
                                legalOppo + "\n\nCommon words by both of you: " + common + "\n\nIllegal words by you: " +
                                illegalClient + "\n\nIllegal words by opponent: " + illegalOppo;

                MessageBox.Show(results);

            }
        }

        /// <summary>
        /// Enables several buttons and disables a few
        /// </summary>
        private void enableAll()
        {
            // Enable all text boxes and buttons for the game
            foreach (Button i in buttons)
                i.Enabled = true;
            foreach (TextBox i in textboxes)
                i.Enabled = true;

            // That includes send, backspace, and the input textbox. Make "connect", "disconnect
            Send.Enabled = true;
            Backspace.Enabled = true;
            button17.Text = "Disconnect";
            textBox1.Enabled = true;

            // Disable the top textboxes
            IP.Enabled = false;
            Nickname.Enabled = false;
        }

        /// <summary>
        /// Inverts the enableAll
        /// </summary>
        private void disableAll()
        {
            // Inversion of enableAll

            foreach (Button i in buttons)
                i.Enabled = false;
            foreach (TextBox i in textboxes)
                i.Enabled = false;
            button17.Text = "Connect";

            Send.Enabled = false;
            Backspace.Enabled = false;
            textBox1.Enabled = false;
            textBox1.Text = "";

            IP.Enabled = true;
            Nickname.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text += button1.Text;
        }


        private void Backspace_Click(object sender, EventArgs e)
        {
            // Delete the most recently input letter
            string result = textBox1.Text;
            if (result.Length == 0) return;
            result = result.Substring(0, result.Length - 1);
            textBox1.Text = result;
        }

        private void Send_Click(object sender, EventArgs e)
        {
            // Send the line and empty the box
            string result = textBox1.Text;
            model.sendLine(result.Trim());
            textBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text += button2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text += button3.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text += button4.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text += button5.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text += button6.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text += button7.Text;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text += button8.Text;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text += button9.Text;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text += button10.Text;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text += button11.Text;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text += button12.Text;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            textBox1.Text += button13.Text;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            textBox1.Text += button14.Text;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            textBox1.Text += button15.Text;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            textBox1.Text += button16.Text;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            // If the game is in progress, this button ends the game. Otherwise, it connects
            if (!model.gameInProgress)
            {
                // If the connection is successful, do stuff
                if (model.attemptConnect(Nickname.Text, IP.Text))
                {
                    model.IncomingLineEvent += received;
                    enableAll();

                    // Build the default player name text for this game.
                    model.playerText = Nickname.Text + " (you): ";

                    // Write the local player text and append a 0.
                    LocalPlayer.Text = model.playerText + "0";


                }
            }
            else
            {
                // Close the model's stuff and reset the display
                model.disconnect();
                model.gameInProgress = false;

                TimeBar.Value = 0;
                TimeLeft.Text = "-----";
                Opponent.Text = "Opponent:";

                disableAll();
            }

        }

    }
}
