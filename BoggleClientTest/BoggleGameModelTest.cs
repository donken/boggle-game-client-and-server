﻿using BoggleModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CustomNetworking;
using ServerUtilities;
using System.Threading;

namespace BoggleClientTest
{
    
    
    /// <summary>
    ///This is a test class for BoggleGameModelTest and is intended
    ///to contain all BoggleGameModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BoggleGameModelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for attemptConnect
        ///</summary>
        [TestMethod()]
        public void Tests()
        {
            // Constructor Test
            BoggleGameModel target = new BoggleGameModel();
            Assert.IsTrue(true);

            // Start the test server
            BoggleServer server = new BoggleServer("100", @"..\..\..\dictionary.txt", "TESTTESTTESTTEST");

            // Attempt Connect tests, Faulty IP
            target = new BoggleGameModel();
            string name = "Test1";
            string IPAddress = "No server here";
            bool expected = false;
            bool actual;
            actual = target.attemptConnect(name, IPAddress);
            Assert.AreEqual(expected, actual);

            Assert.IsFalse(target.attemptConnect(name, IPAddress));

                // Disconnect Test
                Thread.Sleep(500);
                target.disconnect();
                Thread.Sleep(500);

                // Faulty IP test
                Assert.IsFalse(target.attemptConnect("Test1", "I AM NOT A SERVER"));

            // Mock Game Test
                BoggleGameModel target1 = new BoggleGameModel();
                BoggleGameModel target2 = new BoggleGameModel();

                target1.IncomingLineEvent += received;
                target2.IncomingLineEvent += received;

                IPAddress = "localhost";

                Assert.IsTrue(target1.attemptConnect(name, IPAddress));
                Thread.Sleep(500);
                Assert.IsTrue(target2.attemptConnect("Test2", IPAddress));

                    // Double connect possiblity
                    Assert.IsFalse(target1.attemptConnect(name, IPAddress));

                // Send Line Test
                Assert.IsTrue(target1.sendLine("WORD TEST"));
                Assert.IsTrue(target2.sendLine("WORD TEST"));

                // Server closing prematurely
                server.CloseServer();
                server = null;

                // Disconnect test and faulty send test
                Thread.Sleep(500);
                target1.disconnect();
                Assert.IsFalse(target1.sendLine("THIS SHOULDN'T WORK"));
                target2.disconnect();

        }

        /// <summary>
        /// Method which is used to bridge the Model and View
        /// </summary>
        /// <param name="s"></param>
        private void received(string s)
        {
            // Split the string
            string[] receive = s.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            int time = 0;
            string characters = "";

            if (receive[0].ToUpper().Equals("START"))
            {
                // Get the board
                characters = receive[1].Trim().ToUpper();
                try
                {
                    // Get the integer
                    time = Int32.Parse(receive[2]);
                }
                catch (FormatException)
                { return; }

                Assert.IsTrue("TESTTESTTESTTEST".Equals(characters));

                return;
            }
        }
    }
}
